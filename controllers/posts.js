import mongoose from "mongoose";
import PostMessage from "../models/postMessage.js";

import User from "../models/users.js";

export const getPosts = async (req, res) => {
  const { page } = req.query;

  try {
    const LIMIT = 8;
    const startIndex = (Number(page) - 1) * LIMIT;
    const total = await PostMessage.countDocuments({});

    const posts = await PostMessage.find().sort({ _id: -1 }).limit(LIMIT).skip(startIndex);

    res.status(200).json({ data: posts, currentPage: Number(page), numberOfPages: Math.ceil(total / LIMIT) });
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

export const getPost = async (req, res) => {
  const { id } = req.params;

  try {
    const post = await PostMessage.findById(id);

    res.status(200).json(post);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

export const getPostsBySearch = async (req, res) => {
  const { searchQuery, tags } = req.query;

  try {
    const title = new RegExp(searchQuery, "i");
    const posts = await PostMessage.find({ $or: [{ title: title }, { tags: { $in: tags.split(",") } }] });

    res.json({ data: posts });
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

export const createPost = async (req, res) => {
  const post = req.body;
  const newPost = new PostMessage({
    ...post,
    creator: req.userId,
    createdAt: new Date().toISOString(),
  });

  let user;
  try {
    user = await User.findById(req.userId);
  } catch (e) {
    return res.status(500).json({ message: "Creating place failed! Try again!" });
  }

  if (!user) {
    return res.status(404).json({ message: "Could not find user for provided id" });
  }

  try {
    const session = await mongoose.startSession();
    session.startTransaction();
    await newPost.save({ session });
    user.posts.push(newPost);
    await user.save({ session });
    await session.commitTransaction();
  } catch (error) {
    return res.status(409).json({ message: error.message });
  }
  res.status(201).json(newPost);
};

export const updatePost = async (req, res) => {
  const { id: _id } = req.params;
  const post = req.body;

  if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send("No post with that id!");

  const updatedPost = await PostMessage.findByIdAndUpdate(
    _id,
    { ...post, _id },
    {
      new: true,
    }
  );

  res.json(updatedPost);
};

export const deletePost = async (req, res) => {
  // const { id } = req.params;
  // if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send("No post with that id!");

  // try {
  //   await PostMessage.findByIdAndRemove(id);
  //   // await
  // } catch (e) {
  //   res.status(404).json({ message: e.message });
  // }

  // res.json({ message: "Post deleted!" });

  const postId = req.params.id;
  let post;
  try {
    post = await PostMessage.findById(postId).populate("creator");
  } catch (e) {
    return res.status(500).json({ message: "Something went wrong, could not delete place." });
  }

  if (!post) return res.status(404).json({ message: "Could not find place for this id." });

  if (post.creator._id.toString() !== req.userId)
    return res.status(403).json({ message: "You are not allowed to delete this place!" });

  try {
    const session = await mongoose.startSession();
    session.startTransaction();
    await post.remove({ session });
    post.creator.posts.pull(post);
    await post.creator.save({ session });
    await session.commitTransaction();
  } catch (e) {
    return res.status(500).json({ message: "Something went wrong, could not delete place." });
  }

  res.status(200).json({ message: "Succesfully deleted place!" });
};

export const likePost = async (req, res) => {
  const { id } = req.params;

  if (!req.userId) return res.json({ message: "Unauthenticated!" });

  if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send("No post with that id!");

  const post = await PostMessage.findById(id);

  const index = post.likes.findIndex((id) => id === String(req.userId));

  if (index === -1) {
    post.likes.push(req.userId);
  } else {
    post.likes = post.likes.filter((id) => id !== String(req.userId));
  }

  const updatedPost = await PostMessage.findByIdAndUpdate(id, post, {
    new: true,
  });

  res.json(updatedPost);
};

export const commentPost = async (req, res) => {
  const { id } = req.params;
  const { value } = req.body;

  if (!req.userId) return res.json({ message: "Unauthenticated!" });

  if (!mongoose.Types.ObjectId.isValid(id)) return res.status(400).json({ message: "Invalid id!" });

  const post = await PostMessage.findById(id);

  post.comments.push(value);

  const updatedPost = await PostMessage.findByIdAndUpdate(id, post, { new: true });

  res.json(updatedPost);
};

export const getPostByUserId = async (req, res) => {
  const userId = req.params.uid;

  let userWithPosts;
  try {
    userWithPosts = await User.findById(userId).populate({
      path: "posts",
    });
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }

  if (!userWithPosts || userWithPosts.posts.length === 0)
    return res.status(404).json({ message: "Could not find a places for the provided user id!" });
  res.json({
    places: userWithPosts.posts.map((place) => place.toObject({ getters: true })),
    creator: userWithPosts.name,
  });
};
