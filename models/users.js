import mongoose from "mongoose";

const userSchema = mongoose.Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  id: { type: String },
  posts: [{ type: mongoose.Types.ObjectId, required: true, ref: "PostMessage" }],
});

export default mongoose.model("User", userSchema);
